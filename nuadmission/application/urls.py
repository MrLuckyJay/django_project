from django.contrib import admin
from django.urls import path,include
from . import views

app_name = 'application'

urlpatterns = [
    path('', views.index, name = 'application'),
    
    path('app/',include([
        path('', views.application_list, name = 'list'),
        path('<int:id>/', views.application_details, name = 'details'),
    ]))
]
