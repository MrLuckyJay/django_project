from django.contrib import admin
from .models import application

class applicationAdmin(admin.ModelAdmin):

    list_display = ('title', 'body', 'created')

    search_fields = ['title', 'body']

    list_filter = ['created']

admin.site.site_header = "BLOG"

admin.site.register(application, applicationAdmin)


