from .models import application
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse



# Create your views here.
def index(request):
    return HttpResponse("Hello World you are at the start point")

def application_list(request):
    application_list = application.objects.all()
    return render(request,'application/application_list.html',{ 'application_list': application_list})

def application_details(request,id):
    app = get_object_or_404(application, id=id)
    return render(request,'application/application_detail.html' ,{ 'application': app})